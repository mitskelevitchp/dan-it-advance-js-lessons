

// problem

// function showInfo(name="", age="", city=""){
//     alert(`Username: ${name}
//     Age ${age}
//     City: ${city}`)
// }

// showInfo("Andrew", undefined, "Miami")



// solve

function showInfo({name="", age="", city=""}){
    alert(`Username: ${name}
    Age ${age}
    City: ${city}`)
}


showInfo({
    city: "London"
})