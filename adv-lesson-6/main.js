


// let data;

// function loadData(callback){
//     setTimeout(
//         function(){
//             data = "Some data!"
//             callback()
//         },
//         4000
//     )
// }


// loadData(function(){
//     console.log(data)
// })


// function loadData(resolve, reject){
//     setTimeout(function(){
//         console.log("Downloaded!")
//         resolve()
//     }, 5000)
// }

// let pr = new Promise(loadData)

// pr.then(() => {console.log("After download!")})




// function loadData(){
//     return new Promise(function(resolve, reject){
//         setTimeout(()=>{console.log("Done!"); resolve()}, 5439)
//     })
// }

// loadData().then(() => console.log("OK!"))


function connectScript(src){
    return new Promise((resolve, reject) => {
        let script = document.createElement('script');
        script.src = src;
        script.onload = () => resolve("Some connection data!");
        script.onerror = () => reject(new Error("Crash!"))
        document.head.append(script);
    }) 
}


// connectScript('./stats.js', function(){
//     processUser()
//     userData()
// })

// connectScript('./stats.js').then(() => {
//     processUser()
//     userData()
// })

let onConnect = connectScript('./statsr.js')

onConnect.then((data) => {console.log("Function one", data)}).catch(() => {
    console.log("This is error!")
})
