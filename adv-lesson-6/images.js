// https://wallpaperaccess.com/full/38192.jpg
// https://wallpaperaccess.com/full/11729.jpg
// https://wallpapercave.com/wp/wp5134484.jpg
// https://cdn.wallpapersafari.com/82/63/eEJK4w.jpg

const container = document.querySelector('.container')

function loadImage(link){
    return new Promise(function(reslove, reject){
        let wrapper = document.createElement("div")
        wrapper.classList.add('image-wrapper')
        let image = document.createElement("img")
        image.classList.add("image")
        wrapper.append(image)
        image.src = link;

        image.onload = () => reslove(wrapper)
    })
}


// function loadImage(link){
//     return new Promise(function(reslove, reject){
//         let image = document.createElement("img")
//         image.classList.add("image")
//         image.src = link;

//         image.onload = () => reslove(image)
//     })
// }

// loadImage("https://wallpaperaccess.com/full/38192.jpg")
// .then((image) => container.append(image))

// let images = [
//     loadImage("https://wallpaperaccess.com/full/38192.jpg"),
//     loadImage("https://wallpaperaccess.com/full/11729.jpg"),
//     loadImage("https://wallpapercave.com/wp/wp5134484.jpg"),
//     loadImage("https://cdn.wallpapersafari.com/82/63/eEJK4w.jpg"),
// ]

// images.forEach(promise => promise.then((image) => container.append(image)))

// let promiseBoss = Promise.all(images)
// promiseBoss.then(results => results.forEach(result => container.append(result)))


// let fastest = Promise.race(images)
// fastest.then(img => container.append(img))



// images.forEach(function(promise){
//     promise.then(function(img){
//         let loader = container.querySelector(".image.loading");
//         let parent = loader.parentElement;
//         loader.remove()
//         parent.append(img)
//     })
// })

loadImage("https://wallpaperaccess.com/full/38192.jpg")
.then(img => {
    container.append(img)
    return loadImage("https://wallpaperaccess.com/full/11729.jpg")
})
.then(img => {
    container.append(img)
    return loadImage("https://wallpapercave.com/wp/wp5134484.jpg")
})
.then(img => {
    container.append(img)
    return loadImage("https://cdn.wallpapersafari.com/82/63/eEJK4w.jpg")
})
.then(img => container.append(img))
