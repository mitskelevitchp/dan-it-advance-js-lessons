function delay(ms){
    return new Promise(function(resolve, reject){
        setTimeout(resolve, ms)
    })
}

let a = delay(5000)

a.then(() => {console.log("After 5 seconds!")})