// factory based class

// function Person(name){

//     return {
//         sayHi(){
//             console.log("Hello from", name);
//         },

//         sayBye(){
//             console.log("Bye from", name);
//         }
//     }
// }



// function based class

// function Person(name){

//     this.sayHi = function(){
//         console.log("Hello from", name);
//     };

//     this.sayBye = function(){
//         console.log("Bye from", name);
//     }
// }



// prototype based class *

// function Person(name){
//     this.name = name;
//     this.id = 3;
// }

// Person.prototype.sayHi = function(){
//     console.log("Hello from", this.name);
// };

// Person.prototype.sayBye = function(){
//     console.log("Bye from", this.name);
// }



// es6 syntax

class Person {
    constructor(name){
        this.name = name;
        this.id = 3;
    }

    set name(value){
        if (value.length >= 3){
            this._name = value;
        }
        else {
            console.error("Empty name field");
            return
        }
    }

    get name(){
        return `Name is ${this._name}`
    }

    sayHi(){
        console.log("Hello from", this.name);
    }

    sayBye(){
        console.log("Bye from", this.name);
    }

    static helloAll(){
        console.log("Hello from all people!")
    }
}

