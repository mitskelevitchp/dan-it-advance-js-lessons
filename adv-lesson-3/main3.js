class Slider {

    static counter = 0;

    constructor(amount){
        this.amount = amount;
        this._currentSlide = 0;
        Slider.counter++;
    }

    nextSlide(){
        console.log("Next slide")
        if (this._currentSlide < this.amount){
            this._currentSlide++;
            console.log("Slide", this._currentSlide)
        }
        else {
            console.log("Error! Last photo")
        }
        
    }

    prevSlide(){
        console.log("Prev slied")
        if (this._currentSlide > 0){
            this._currentSlide--;
            console.log("Slide", this._currentSlide)
        }
        else {
            console.log("Error! First photo!")
        }
        
    }

    get slide(){
        return this._currentSlide
    }

    set slide(value){
        if (value >= 0 && value <= this.amount){
            this._currentSlide = value;
        }
        else {
            console.log("No such photo...");
        }
    }
}

class ProductSlider extends Slider {
    constructor(amount, title){
        super(amount)
        this.title = title
    }

    changeTitle(newTitle){
        this.title = newTitle;
        console.log("Title changed!")
    }

    nextSlide(){
        console.log("Next ProductSlide")
        if (this._currentSlide < this.amount){
            this._currentSlide++;
            console.log("Slide", this._currentSlide)
        }
        else {
            console.log("Error! Last photo")
        }
        
    }

    prevSlide(){
        console.log("Prev ProductSlied")
        if (this._currentSlide > 0){
            this._currentSlide--;
            console.log("Slide", this._currentSlide)
        }
        else {
            console.log("Error! First photo!")
        }
    }
}



let s1 = new Slider(10);
let s2 = new ProductSlider(4, "Hello1");

