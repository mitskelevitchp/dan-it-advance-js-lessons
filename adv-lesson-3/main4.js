class Printer {

    static globalData = null;

    constructor(data){
        this.data = data
    }

    print(){
        if (Printer.globalData == null){
            console.log(this.data);
        }
        else {
            console.log(Printer.globalData)
        }
        
    }

    static resetGlobalData(){
        Printer.globalData = null;
    }
}


let p1 = new Printer("Hello");
let p2 = new Printer("Apple");

