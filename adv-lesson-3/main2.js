// prototype based inheritense

// class user
function User(username){
    this.username = username
}

User.prototype.sendMessage = function(){
    console.log(`Message from ${this.username}`)
}


// class Admin

function Admin(username){
    this.username = username
}

Admin.prototype.block = function(){
    console.log(`Admin ${this.username} block user!`)
}


Admin.prototype.__proto__ = User.prototype;

