let hamster = {
    stomach: [],

    eat(food){
        if (!this.hasOwnProperty('stomach')){
            this.stomach = [];
        }
        this.stomach.push(food);
        
    }
}


let lazy = {
    __proto__: hamster
}

let speedy = {
    __proto__: hamster
}