

// let user = {
//     name: "Maksim",
//     age: 45,
//     city: "Miami",


//     sayHello(){
//         console.log(`Hello from ${this.name}`)
//     },


//     // ....
// }


// function User(name, age, city){
//     this.name = name;
//     this.age = age;
//     this.city = city;

//     this.sayHello = function(){
//         console.log(`Hello from ${this.name}`);
//     }
// }


function Car(model){
    this.model = model;
    this.fuelLevel = 0;
    this.isRunning = false;
    this.fuelType = ['A95', 'A98', 'A100']

    this.startEngine = function(){
        if (this.fuelLevel > 0){
            console.log("Engine started!");
            this.isRunning = true;
        }
        else {
            console.log("No fuel");
        }
    };


    this.addFuel = function(amount){
        this.fuelLevel += amount;
    }
}


function FuelStation(){
    this.fuel = 1000;

    this.refillCar = function(carObj, amount){
        carObj.addFuel(amount);
        this.fuel -= amount;
    }
}


let car1 = new Car("KIA");
let car2 = new Car("AUDI");

let azs = new FuelStation();