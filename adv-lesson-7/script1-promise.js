


function getProducts(){
    let dataLink = "https://dummyjson.com/products"
    let request = new XMLHttpRequest()
    request.open('GET', dataLink)

    return new Promise(function(resolve, reject){
        request.onload = () => {
            let json = JSON.parse(request.responseText)
            resolve(json.products)
        }
        request.send()
    })
}


let promiseRequest = getProducts();
promiseRequest.then(data => console.log(data))