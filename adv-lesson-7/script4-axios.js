// axios.get("https://dummyjson.com/users").then(response => {
//     if (response.status == 200){
//         response.data.users.forEach(user => console.log(user))
//     }
// })

function getAllUsers(){
    return axios.get("https://dummyjson.com/users")
}


function getAllProducts(){
    return axios.get("https://dummyjson.com/products")
}


let requests = [
    getAllProducts(),
    getAllUsers()
]

axios.all(requests).then(responses => {
    console.log("users: ", responses[1].data.users),
    console.log("products: ", responses[0].data.products)
})