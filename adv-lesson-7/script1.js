let dataLink = "https://dummyjson.com/products";


const list = document.querySelector(".products")

// XHR
let request = new XMLHttpRequest()

request.onload = () => {
    let products = JSON.parse(request.responseText).products
    products.forEach(product => {
        const li = document.createElement('li')
        li.textContent = `Name: ${product.title} | Price: ${product.price}`
        list.append(li)
    })
}

// request.onreadystatechange = () => {
//     console.log(request.readyState)
// }

request.open('GET', dataLink)

request.send()

