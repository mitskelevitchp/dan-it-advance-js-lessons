const geo = document.querySelector('.location')
const temp = document.querySelector('.temp')
const wind = document.querySelector('.wind')


const API_KEY = "f22b48abdd4943b2af6165250211510"


let form = document.forms.weather

function loadWeather(city){
    const URL = `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${city}`

    fetch(URL)
        .then(response => {
            if (response.status != 200){
                throw new Error("No such city")
            }
            return response.json()
        })
        .then(data => {
            geo.textContent = "City: " + data.location.region
            temp.textContent = "Temp: " + data.current.temp_c
            wind.textContent = "Wind speed: " + data.current.wind_kph
        })
        .catch(error => {
            console.log(error)
            alert("Incorrect city name. Try again!")
        })
        
}

form.addEventListener('submit', event => {
    event.preventDefault();
    let city = form.elements.city.value;
    loadWeather(city);
})