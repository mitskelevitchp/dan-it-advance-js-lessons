
class Car {
    constructor(model){
        this.model = model;
        this._fuel = 0;
    }

    get fuel(){
        return this._fuel
    }

    set fuel(value){
        if (value >= 0 && value <= 60){
            this._fuel = value
        }
        else {
            throw {
                inputValue: value,
                currentValue: this.fuel,
                description: "Fuel amount error!"
            }
        }
    }
}

let c1 = new Car("KIA");

let amount = Number(prompt("Enter fuel:"))

try {
    c1.fuel = amount;
}catch(e){
    alert(`Incorrect value. Your value is ${e.inputValue}`)
    console.log("Value was incorrect! Error =>", e)
}
