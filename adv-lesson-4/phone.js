class IncorrectPhoneNumber extends Error {
    constructor(){
        super("Incorrect number. Check amount of numbers")
        this.name = "IncorrectPhoneError"
    }
}



class Phone {
    constructor(operator, balance){
        this.operator = operator
        this.balance = balance
    }

    call(phoneNumber){
        if (phoneNumber.length < 10){
            throw new IncorrectPhoneNumber()
        }
        else {
            console.log("Calling....")
        }
    }

    
}


let p1 = new Phone("Kyivstar", 345)
try {
    p1.call("097432651")
}
catch(e){
    console.log("Check your number")
}
