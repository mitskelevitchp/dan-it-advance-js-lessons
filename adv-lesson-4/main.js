
function createStorage(initialValue){
    let value = initialValue;

    return [
        function() {
            return value;
        },

        function(newValue){
            value = newValue;
        }
    ]
}


let [getValue, setValue] = createStorage(10);