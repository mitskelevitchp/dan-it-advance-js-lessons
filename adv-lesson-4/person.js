class NotValidAgeError extends Error {
    constructor(message, age){
        super(message)
        this.age = age
        this.name = "NotValidAgeError"
    }
}


class CriticalError extends Error {
    constructor(msg){
        super(msg)
        this.name = "Critical Error!"
    }
}




class Person {
    constructor(name, age){
        if (name.length == 0){
            throw new CriticalError("Critical")
        }
        this.name = name
        if (age < 1 || age > 100){
            throw new NotValidAgeError("Error with age", age)
        }
        this.age = age
    }

    happyBirthday(){
        this.age++;
    }

}

try {
    let p = new Person("kdmfkdf", 123)
}
catch(e){
    if (e instanceof NotValidAgeError){
        console.log("Input correct age")
    }
    else {
        throw e
    }
}


